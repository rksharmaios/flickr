//
//  AppDelegate.swift
//  FlickerPhotoSearch
//
//  Created by Rakesh Sharma on 14/09/20.
//  Copyright © 2020 Rakesh Sharma. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let mode = NaiveDarkAndLightMode.current()
        NaiveDarkAndLightMode.applyMode(mode: mode)
        
        return true
    }
}

