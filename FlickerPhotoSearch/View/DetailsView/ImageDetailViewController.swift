//
//  ImageDetailViewController.swift
//  FlickerPhotoSearch
//
//  Created by Rakesh Sharma on 25/06/2020.
//  Copyright © 2020 Rakesh Sharma. All rights reserved.
//

import UIKit

final class ImageDetailViewController: BaseViewController {
    
    @IBOutlet weak var imgScrollView: ImageScrollView!

    
    //MARK:- Properties
    var viewModel : FlickerPhotoCellViewModel?
    
     //MARK:- View Controller's Life Cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel?.title
        imgScrollView.setup()
        
        imgScrollView.display(image: (viewModel?.image)!)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
    }
    
     
}
