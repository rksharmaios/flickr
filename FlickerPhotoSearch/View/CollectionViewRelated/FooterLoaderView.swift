//
//  FooterLoaderView.swift
//  FlickerPhotoSearch
//
//  Created by Rakesh Sharma on 14/09/2020.
//  Copyright © 2020 Rakesh Sharma. All rights reserved.
//

import UIKit

final class FooterLoaderView: UICollectionReusableView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
